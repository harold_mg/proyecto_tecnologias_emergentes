import React,{Component} from 'react';
import { Meteor } from 'meteor/meteor';
import {Tracker} from 'meteor/tracker'
import { render } from 'react-dom';
import {routes,checkAuthUser} from '/imports/routes/Routes'
import Principal from '/imports/view/principal/Principal'
import SwitchRoutes from '/imports/routes/SwitchRoutes'
import {BrowserRouter,Route,Switch,Redirect} from 'react-router-dom'


T9n = require ('meteor-accounts-t9n').T9n

Tracker.autorun(function(){
  T9n.map('es', require('meteor-accounts-t9n/build/es'))
  T9n.setLanguage('es')

  const authenticated = !!Meteor.userId()

  checkAuthUser(authenticated)

  if(!authenticated){
    Meteor.logout(function(err) {

    }); 
  }
})

class App extends Component {
  render() {
    return (
      <BrowserRouter>
          <Switch>

              <Route exact path="/">
                  <Redirect to="/bienvenido/principal"/>
              </Route>
              <Route exact path="/bienvenido">
                  <Redirect to="/bienvenido/principal"/>
              </Route>
              {                
                routes.map((route,i)=>{
                  return <SwitchRoutes key={i} {...route}/>
                })
              }
          </Switch>
      </BrowserRouter>
    )
  }
}
Meteor.startup(() => {
  render(<App/>, document.getElementById('react-target'));
});