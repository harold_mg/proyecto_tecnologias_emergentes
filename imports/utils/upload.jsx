import { Meteor } from 'meteor/meteor';
import UserFiles from '../models/files/class'

export default class uploadFiles{
    constructor(file,self){
        this.file = file,
        this.self = self
    }
    newUpload = (callback)=>{
        const self = this.self 
        const files = this.file 

        if(files && files[0]){
            var file = files[0];
            if(file){
                let uploadInstance = UserFiles.insert({
                    file:file,
                    meta:{
                        locator:self.props.fileLocator,
                        userId:Meteor.userId()
                    },
                    chunkSize:'dynamic',
                    allowWebWorkers:true,
                },false)
                self.setState({
                    uploading:uploadInstance,
                    inProgress:true
                })

                uploadInstance.on('start',function(){
                    console.log('Iniciando subida de archivo')
                })

                uploadInstance.on('end',function(error,fileObj){
                    callback(null,fileObj)
                })

                uploadInstance.on('uploaded',function(error,fileObj){
                        console.log('subiendo: ',fileObj)                    
                        self.refs['fileinput'].value = ''
                        self.setState({
                            uploading:[],
                            progress:0,
                            inProgress:false
                        })
                })

                uploadInstance.on('error',function(error,fileObj){
                    console.log('error al subir el archivo '+error)
                    callback(error,null)
                })

                uploadInstance.on('progress',function(progress,fileObj){
                    console.log('porcentaje de subida '+ progress)
                    self.setState({
                        progress:progress
                    })
                })

                uploadInstance.start()
            }
        }
    }
}
