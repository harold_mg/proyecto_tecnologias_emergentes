
import React, { Component } from 'react';
import Principal from '/imports/view/principal/Principal'
import Main from '/imports/view/principal/Main'
import Signin from '/imports/view/auth/Signin'
import Signup from '/imports/view/auth/Signup'
import Layout from '/imports/view/layout/Layout'
import Home from '/imports/view/home/Home'
////
import About from '/imports/view/about/About'
import Carta from '/imports/view/carta/Carta'
////
//import CreatePublication from '/imports/view/publications/CreatePublication'
//import userPublications from '/imports/view/publications/userPublications'
import {useLocation} from "react-router-dom";

import { createBrowserHistory } from "history";
import CategoryRoutes from './CategoryRoutes'
import PublicationsRoutes from './PublicationsRoutes'

const history = createBrowserHistory();

function NoMatch() {
    let location = useLocation();  
    return (
      <div>
        <h3>
          La pagina solicitada no existe <code>{location.pathname}</code>
        </h3>
      </div>
    );
  }
  
export const routes = [
    {
        path:'/bienvenido',
        component:Principal,
        authenticated:false,
        routes:[
            {
                path:'/bienvenido/principal',
                component:Main
            },
      
            {
                path:'/bienvenido/login',
                component:Signin
            },
            {
                path:'/bienvenido/register',
                component:Signup
            },
            {
              path:'/bienvenido/about',
              component:About
            },
            {
              path:'/bienvenido/menu',
              component:Carta
            },
        ]
    },
    {
        path:'/dashboard',
        component:Layout,
        authenticated:true,
        routes: [
            {
                path: "/dashboard/home",
                component: Home
            },  
            ...PublicationsRoutes,
            ...CategoryRoutes
        ]
    },
    {
      path:'/otraruta',
      component:Layout,
      authenticated:true
    },
    {
        path:'*',
        component:NoMatch
    }
  
]

function getAllRoutesAutheticated(routes){
    let routesAuthenticate = []
    routes.forEach((value,index) => {
        if(JSON.stringify(value.authenticated) === JSON.stringify(true)){
          routesAuthenticate.push(value.path)
          if(value.routes){
            value.routes.forEach((v,i) => {
              routesAuthenticate.push(v.path)
            });
          }
        }
    });
    return routesAuthenticate
  }
  

export const checkAuthUser = function(authenticated){
    const path = history.location.pathname
    const isAuthenticatedPage = getAllRoutesAutheticated(routes).includes(path)
    if(authenticated && isAuthenticatedPage){
        history.replace('/dashboard/home')
    }else if(!authenticated && isAuthenticatedPage){
      history.replace('/')
      location.reload()
    }
}
