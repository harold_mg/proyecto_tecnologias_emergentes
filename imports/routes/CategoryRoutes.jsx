import CreateCategory from '../view/category/CreateCategory'
import allCategory from '../view/category/allCategory'

export default routes = [
    {
      path:'/dashboard/create-category',
      component:CreateCategory
    },
    {
      path:'/dashboard/all-category',
      component:allCategory
    },

]
