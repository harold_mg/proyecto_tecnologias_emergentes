import React from 'react'
import {Route,Redirect} from 'react-router-dom'

const SwitchRoutes = (route) =>{
    const auth = !!Meteor.userId()
    return (
        <Route
            path={route.path}
            render={(props)=>{
                if(route.authenticated){
                    if(auth){
                        {console.log(props)}
                        return <route.component {...props} routes={route.routes} />
                    }else{
                        return <Redirect to='/bienvenido/login'/>
                    }
                }else{
                    if(auth && route.path == '/bienvenido/login'){
                        return window.location.replace('/dashboard/home')
                    }
                    else{
                        return <route.component {...props} routes={route.routes} />
                    }
                        
                }
            }}
        />
    )
}

export default SwitchRoutes
