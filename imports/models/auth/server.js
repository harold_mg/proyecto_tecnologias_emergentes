Meteor.methods({
    userCreate:function(form){
        
        if(form.password != form.re_password)
            throw new Meteor.Error(406, "La constraseña debe ser igual a la confirmacion de contraseña");
        Accounts.createUser({
            createdAt:new Date(),//fecha de creacion del usuario,
            username:form.username,
            email:form.email,
            password:form.password,
            profile:{
            fullname:form.nameComplete
            }
        })
        
        return 'Usuario Creado'
    }
})
