import { Meteor } from 'meteor/meteor';
import { FilesCollection } from 'meteor/ostrio:files';

export default UserFiles = new FilesCollection({
    //storagePath: '/sis414/files',
    storagePath: '../../../../../sis414/files',
    downloadRoute: '/files/sis414',
    collectionName: 'UserFiles',
    permissions: 0o755,
    allowClientCode: false,
    cacheControl: 'public, max-age=31536000',
    onbeforeunloadMessage() {
        return 'Upload is still in progress! Upload will be aborted if you leave this page!';
    },
    onBeforeUpload(file) {

        if (file.size <= 10485760 && /png|mp4|jpe?g/i.test(file.ext)) {
          return true;
        }
        return 'Please upload image, with size equal or less than 10MB';
    },
    downloadCallback(fileObj) {
        if (this.params.query.download == 'true') {

          UserFiles.update(fileObj._id, {$inc: {'meta.downloads': 1}});
        }

        return true;
    }
});
