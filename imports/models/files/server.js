import { Meteor } from 'meteor/meteor';
import UserFiles from './class'

Meteor.methods({
    RemoveFile(id){
        UserFiles.remove({_id:id},function(error){
            if(error){
                console.log('no se pudo remover el archivo'+ error.reason)
            }else{
                console.log('eliminado con exito')
            }
        })
    }
})

Meteor.publish('files.all',function(){
    return UserFiles.find().cursor
})
