import {publicationsClass} from './class'

export const queryPublications = class{
    constructor(options,mthis){
        this.options = options
        this.mthis = this
    }
    getPublications(){
        try {
            return publicationsClass.find({user:Meteor.userId()},{sort:{createdAt:-1}})
        } catch (error) {
            console.log(error)
        }
    }
    getOnePublication(){
        try {
            console.log(this.options)
            if(this.options.pub)
                return publicationsClass.find({_id:this.options.pub,user:Meteor.userId()},{sort:{createdAt:-1}})
        } catch (error) {
            console.log(error)
        }
    }
    getAllPublications(){
        try {
            
            if(this.options.category){
                return publicationsClass.find({active:true,idCategory:this.options.category._id},{sort:{createdAt:-1}})
            }else{
                return publicationsClass.find({active:true},{sort:{createdAt:-1}})
            }
        } catch (error) {
            console.log(error)
        }
    }

}

