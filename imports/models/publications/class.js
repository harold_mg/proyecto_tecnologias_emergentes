import { Class } from 'meteor/jagi:astronomy'; 
const publicationsCollection = new Mongo.Collection('publications',{idGeneration:'MONGO'});
export const publicationsClass = Class.create({
  name: 'publicationsClass',
  collection: publicationsCollection,
  fields: {
      title:{
        type:String,

      },
      description:{
          type:String,
      },
      phone:{
          type:String,
      },
      price:{
          type:Number
      },
      startDate:{
          type:Date
      },
      endDate:{
          type:Date
      },
      nameCategory:{
          type:String
      },
      idfile:{
          type:String
      },
      urlfile:{
          type:String
      },
      active:{
          type:Boolean,
          default:true
      },
      idCategory:{
        type:Mongo.ObjectID
      },
      username: String,
      created_view:{
        type:Date,
      }
    }
  });
  
  if (Meteor.isServer) {
      publicationsClass.extend({
        fields: {
          user: String,

        },
        behaviors: {
          timestamp: {
            hasCreatedField: true,
            createdFieldName: 'createdAt',
            hasUpdatedField: true,
            updatedFieldName: 'updatedAt'
          }
        }
      });
  }