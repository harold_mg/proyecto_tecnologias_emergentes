import React, { Component } from 'react'
import {publicationsClass} from '../../models/publications/class' 
import {categoryClass} from '../../models/category/class'
import { withTracker } from 'meteor/react-meteor-data'
import { Meteor } from 'meteor/meteor';
import DatePicker from "react-datepicker";
import FileUpload from '../../components/FileUpload'
import uploadFiles from '../../utils/upload'


class CreatePublication extends Component {
    constructor(props){
        super(props)
        this.state={
            form:{
                title:null,
                description:null,
                phone:null,
                price:null,
                startDate:new Date(),
                endDate:new Date(),
                image:null,
                category:null
            },
            errors:{}
        }
    }
    validateForm=()=>{
        const {form} = this.state 
        let errorsform = {}
        let formsIsValid = true
        if(!form.title){
            formsIsValid = false
            errorsform['title'] = "El titulo no puede estar vacio"
        }
        if(!form.description){
            formsIsValid = false
            errorsform['description'] = "la description no puede estar vacio"
        }
        if(!form.phone){
            formsIsValid = false
            errorsform['phone'] = "el telefono no puede estar vacio"
        }
        if(!form.price){ 
            formsIsValid = false
            errorsform['price'] = "el precio no puede estar vacio"
        }
        if(!form.startDate){ 
            formsIsValid = false
            errorsform['startDate'] = "la fecha inicial no puede estar vacio"
        }
        if(!form.endDate){ 
            formsIsValid = false
            errorsform['endDate'] = "la fecha final no puede estar vacio"
        }
        this.setState({errors:errorsform}) 
        return formsIsValid
    }
    
    
    createNewPublications=(e)=>{ 
        e.preventDefault()
        if(this.validateForm()){
            
            const newpublicacion = new publicationsClass()
            const {form} = this.state
            form.price = parseInt(form.price)
            
            const {form:{image}} = this.state 
            const uf = new uploadFiles(image.file,image.self) 
            uf.newUpload(
                function(error,success){
                   if(error){
                    console.log('*********************************')
                    console.log(error)
                    console.log('*********************************')
                   }else{
                        form.image = success._id
                        newpublicacion.callMethod('newPublication',form,(error,result)=>{ 
                            if(error){
                                alert(error)
                            }else{
                                alert(result)
                                document.getElementById("newPublication").reset()
                            }
                        })       
                   }
                }
            )

        }else{
           alert('el formulario tiene errores')
        }
    }
    changeTextInput = (e) =>{ 
        const value = e.target.value 
        const property = e.target.name

        
        this.setState(prevState=>(
            {form:{
                    ...prevState.form,
                    [property]:value,
                }
            }
        ))

    }
    changeDateInput = (type,date) =>{
        this.setState(prevState=>(
            {form:{
                    ...prevState.form,
                    [type]:date,
                }
            }
        ))
    }
    changeSelectInput =(e)=>{
        const value = e.target.value
        this.setState(prevState=>(
            {form:{
                    ...prevState.form,
                    category:value,
                }
            }
        ))
    }
    changeFileInput =(data)=>{

        const inputfile = data.file
        if(inputfile && inputfile[0]){
            console.log(inputfile[0])
            let reader = new FileReader()
            reader.onload = function(v){
                $('#previewimage').attr('src',v.target.result)
            }
            reader.readAsDataURL(inputfile[0])

            this.setState(prevState=>(
                {form:{
                        ...prevState.form,
                        image:data,
                    }
                }
            ))
        }
    }
    render() {
        const {errors} = this.state
        const {categorys,subcriptionCategory} = this.props
        return (
            <div>
                <section className="section">
                    <div className="section-body">
                        <div className="row">
                            <div className="col-12 col-md-6 col-lg-12">
                                <div className="card">
                                    <div className="card-header">
                                        <h4>Crear Nueva Publicacion</h4>
                                    </div>
                                    {!subcriptionCategory.ready()?
                                        <h1>Cargando!!!</h1>
                                    :
                                        <div className="card-body">
                                            <form onSubmit={this.createNewPublications} id="newPublication">
                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <div className="form-group">
                                                            <label>Titulo de publicacion</label>
                                                            <input type="text" className={errors.title?"form-control is-invalid":"form-control"} name={'title'} onChange={this.changeTextInput} autoComplete="off"/>
                                                            {errors.title?<div className="invalid-feedback">{errors.title}</div>:null}
                                                        </div>
                                                        <div className="form-group">
                                                            <label>Descripcion</label>
                                                            <textarea type="text" className={errors.description?"form-control invoice-input is-invalid":"form-control invoice-input"}  name={'description'} onChange={this.changeTextInput}>
                                                            </textarea>
                                                            {errors.description?<div className="invalid-feedback">{errors.description}</div>:null}
                                                        </div>
                                                        <div className="form-group">
                                                            <label>Numero de Telefono</label>
                                                            <div className="input-group">
                                                                <div className="input-group-prepend">
                                                                    <div className="input-group-text">
                                                                        <i className="fas fa-phone"></i>
                                                                    </div>
                                                                </div>
                                                                <input type="text" className={errors.title?"form-control phone-number is-invalid":"form-control phone-number"} name={'phone'} onChange={this.changeTextInput} autoComplete="off"/>
                                                                {errors.phone?<div className="invalid-feedback">{errors.phone}</div>:null}
                                                            </div>
                                                        </div>
                                                        <div className="form-group">
                                                            <label>Precio</label>
                                                            <div className="input-group">
                                                                <div className="input-group-prepend">
                                                                    <div className="input-group-text">
                                                                        $
                                                                    </div>
                                                                </div>
                                                                <input type="text" className={errors.price?"form-control currency is-invalid":"form-control currency"} name={'price'} onChange={this.changeTextInput} autoComplete="off"/>
                                                                {errors.price?<div className="invalid-feedback">{errors.price}</div>:null}
                                                            </div>
                                                        </div>
                                                        <div className="form-group">
                                                            <label>Seleccione una Categoria</label>
                                                            <select className="form-control" onChange={this.changeSelectInput}>
                                                                <option defaultValue>Seleccione una categoria</option>  
                                                                {
                                                                    categorys.map((category,key)=>{
                                                                        return <option key={`category ${key}`} value={category._id}>{category.name}</option>
                                                                    })
                                                                }
                                                            </select>
                                                        </div>
                                                        <div className="form-group">
                                                            <label className="form-label">Archivo</label>
                                                            
                                                            <FileUpload changeFileInput={this.changeFileInput}/>
                                                        </div>
                                                        <div className="form-group">
                                                            <label>Fecha de Publicacion</label>
                                                            
                                                            <div>
                                                                <DatePicker selected={this.state.form.startDate} className={errors.startDate?"form-control datemask is-invalid":"form-control datemask"} name={'startDate'} 
                                                                    onChange={date => {
                                                                        this.changeDateInput('startDate',date)
                                                                    }}
                                                                />
                                                            </div>
                                                            {errors.startDate?<div className="invalid-feedback" style={{display:'block'}}>{errors.startDate}</div>:null}
                                                        </div>
                                                        <div className="form-group">
                                                            <label>Fecha de Culminacion</label>
                                                            <div>
                                                                <DatePicker selected={this.state.form.endDate} className={errors.startDate?"form-control datemask is-invalid":"form-control datemask"} name={'endDate'} 
                                                                    onChange={date => {
                                                                        this.changeDateInput('endDate',date)
                                                                    }}
                                                                />
                                                            </div>
                                                            {errors.endDate?<div className="invalid-feedback" style={{display:'block'}}>{errors.endDate}</div>:null}
                                                            
                                                        </div>
                                            
                                                    </div>
                                                    <div className="col-md-6 d-flex justify-content-center">
                                                        <div className="author-box-center">
                                                            <img alt="image" src="/dashboard/img/users/Images-icon.png"  id="previewimage" className="rounded-circle author-box-picture" style={{width: '100%',height:'500'}} />
                                                            <div className="clearfix" />
                                                            <div className="author-box-name">
                                                                
                                                            </div>
                                                            <div className="author-box-job d-flex justify-content-center">Vista Previa</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <button type="submit" className="btn btn-lg btn-dark">ACEPTAR</button>
                                            </form>
                                        </div>
                                    }
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

export default withTracker((props)=>{
    const subcriptionCategory = Meteor.subscribe('category',{},'getCategory')
    const categorys = categoryClass.find().fetch()
    return {categorys,subcriptionCategory}
})(CreatePublication)



