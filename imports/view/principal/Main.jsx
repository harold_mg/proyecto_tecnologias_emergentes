import React, { Component } from 'react'

import { withTracker } from 'meteor/react-meteor-data'
import {publicationsClass} from '../../models/publications/class'
import {categoryClass} from '../../models/category/class'

const categorySelect = new ReactiveVar(undefined)

class Main extends Component {
    selectCategory = (category) => {
        categorySelect.set(category?category:undefined)
    }
    render() {
        const {publications,subscriptionPublications,categorys,publicationsLimitFour} = this.props
        
        return (
            <div>
                
                <section className="home-slider owl-carousel img" style={{backgroundImage: `url("/images/bg_1.jpg")` }}>
                    <div className="slider-item">
                        <div className="overlay"></div>
                        <div className="container">
                            <div className="row slider-text align-items-center" data-scrollax-parent="true">

                                <div className="col-md-6 col-sm-12 ftco-animate">
                                    <span className="subheading">BIENVENIDO</span>
                                    <h1 className="mb-4">PIZZA ITALIANA</h1>
                                    <p className="mb-4 mb-md-5">A small river named Duden flows by their place and
                                        supplies it with the
                                        necessary regelialia.</p>
                                    <p><a href="#" className="btn btn-primary p-3 px-xl-4 py-xl-3">ordenar ahora</a> <a
                                            href="/bienvenido/menu" className="btn btn-white btn-outline-white p-3 px-xl-4 py-xl-3">ver
                                            Menu</a></p>
                                </div>
                                <div className="col-md-6 ftco-animate">
                                    <img src="/images/bg_1.png" className="img-fluid" alt="" />
                                </div>

                            </div>
                        </div>
                    </div>

                    <div className="slider-item">
                        <div className="overlay"></div>
                        <div className="container">
                            <div className="row slider-text align-items-center" data-scrollax-parent="true">

                                <div className="col-md-6 col-sm-12 order-md-last ftco-animate">
                                    <span className="subheading">BIENVENIDO</span>
                                    <h1 className="mb-4">PIZZA POTOSINA</h1>
                                    <p className="mb-4 mb-md-5">A small river named Duden flows by their place and
                                        supplies it with the
                                        necessary regelialia.</p>
                                    <p><a href="#" className="btn btn-primary p-3 px-xl-4 py-xl-3">Ver Promoción</a> 
                                    <a href="/bienvenido/menu" className="btn btn-white btn-outline-white p-3 px-xl-4 py-xl-3">Ver Menu</a></p>
                                </div>
                                <div className="col-md-6 ftco-animate">
                                    <img src="/images/bg_2.png" className="img-fluid" alt="" />
                                </div>

                            </div>
                        </div>
                    </div>

                    <div className="slider-item" style={{ backgroundImage: `url("/images/bg_3.jpg")`}}>
                        <div className="overlay"></div>
                        <div className="container">
                            <div className="row slider-text justify-content-center align-items-center"
                                data-scrollax-parent="true">

                                <div className="col-md-7 col-sm-12 text-center ftco-animate">
                                    <span className="subheading">BIENVENIDO</span>
                                    <h1 className="mb-4">AL RINCON DE GASTRONOMIA POTOSINA</h1>
                                    <p className="mb-4 mb-md-5">A small river named Duden flows by their place and
                                        supplies it with the
                                        necessary regelialia.</p>
                                    <p><a href="#" className="btn btn-primary p-3 px-xl-4 py-xl-3">Ver Novedades</a> <a
                                            href="/bienvenido/menu"
                                            className="btn btn-white btn-outline-white p-3 px-xl-4 py-xl-3">Ver
                                            Menu</a></p>
                                </div>

                            </div>
                        </div>
                    </div>
                </section>

                <section className="ftco-intro">
                    <div className="container-wrap">
                        <div className="wrap d-md-flex">
                            <div className="info">
                                <div className="row no-gutters">
                                    <div className="col-md-4 d-flex ftco-animate">
                                        <div className="icon"><span className="icon-phone"></span></div>
                                        <div className="text">
                                            <h3>000 (123) 456 7890</h3>
                                            <p>A small river named Duden flows</p>
                                        </div>
                                    </div>
                                    <div className="col-md-4 d-flex ftco-animate">
                                        <div className="icon"><span className="icon-my_location"></span></div>
                                        <div className="text">
                                            <h3>198 West 21th Street</h3>
                                            <p>Suite 721 New York NY 10016</p>
                                        </div>
                                    </div>
                                    <div className="col-md-4 d-flex ftco-animate">
                                        <div className="icon"><span className="icon-clock-o"></span></div>
                                        <div className="text">
                                            <h3>Open Monday-Friday</h3>
                                            <p>8:00am - 9:00pm</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="social d-md-flex pl-md-5 p-4 align-items-center">
                                <ul className="social-icon">
                                    <li className="ftco-animate"><a href="#"><span className="icon-twitter"></span></a>
                                    </li>
                                    <li className="ftco-animate"><a href="#"><span className="icon-facebook"></span></a>
                                    </li>
                                    <li className="ftco-animate"><a href="#"><span
                                                className="icon-instagram"></span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="ftco-about d-md-flex">
                    <div className="one-half img" style={{ backgroundImage: `url("/images/about.jpg")` }}>
                    </div>
                    <div className="one-half ftco-animate">
                        <div className="heading-section ftco-animate ">
                            <h2 className="mb-4">BIENVENIDO<span className="flaticon-pizza">A RIQUISIMA</span>
                            </h2>
                            <h3>Una Página dedicada al gourmet Potosino </h3>
                        </div>
                        <div>
                            <p>On her way she met a copy. The copy warned the Little Blind Text, that where it came from
                                it would have
                                been rewritten a thousand times and everything that was left from its origin would be
                                the word "and" and
                                the Little Blind Text should turn around and return to its own, safe country. But
                                nothing the copy said
                                could convince her and so it didn’t take long until a few insidious Copy Writers
                                ambushed her, made her
                                drunk with Longe and Parole and dragged her into their agency, where they abused her for
                                their.</p>
                        </div>
                    </div>
                </section>

                <section className="ftco-section">
                    <div className="container">
                        <div className="row justify-content-center mb-5 pb-3">
                            <div className="col-md-7 heading-section ftco-animate text-center">
                                <h2 className="mb-4">Vea las novedades en delicias para toda la familia</h2>
                                <p>Por favor verifique las fechas ya que algunos productos ya no tengan la promoción anunciada.</p>
                            </div>
                        </div> 
                        <div className="row d-flex">
                            {
                                publications.map((data,key)=>{
                                    return <div key={`article_${key}`} className="col-md-4 d-flex">
                                               <div className="blog-entry align-self-stretch" style={{width:'100%'}}>
                                                    <a href="/" className="block-20"
                                                        style={{ backgroundImage: `url("${data.urlfile}")` }}>
                                                    </a>
                                                    <div className="text py-4 d-block">
                                                        <div className="meta">                                      
                                                            <a href="#" style={{'fontSize': '30px'}}>A TAN SOLO: {data.price}bs</a>
                                                            <a href="#">
                                                                <h4>Desde el: {moment(data.created_view).format('dddd DD [de] MMMM')}</h4>
                                                                <h4>Hasta el: {moment(data.endDate).format('dddd DD [de] MMMM')}</h4>
                                                                {/*<p>{moment(data.created_view).format('MMM')}</p>*/}
                                                            </a>
                                                            <a href="#">Usuario: {data.username}</a>                                                                     
                                                            <div></div>
                                                            <div><a href="#" className="meta-chat"><span className="icon-chat"></span>
                                                                    3</a></div>
                                                        </div>
                                                        <h3 className="heading mt-2"><a href="#">{data.title}</a></h3>
                                                        <p>{data.description}</p>
                                                    </div>
                                                </div>
                                            </div>                                                           
                                })
                            }                                                     
                        </div>
                    </div>
                </section>

  
                <section className="ftco-menu">
                    <div className="container-fluid">
                        <div className="row d-md-flex">
                            <div className="col-lg-4 ftco-animate img f-menu-img mb-5 mb-md-0"
                                style={{ backgroundImage: `url("/images/about.jpg")` }}>
                            </div>
                            <div className="col-lg-8 ftco-animate p-md-5">
                                <div className="row">
                                    <div className="col-md-12 nav-link-wrap mb-5">
                                        <h3>Categorias de nuestros productos</h3>
                                        <div className="nav ftco-animate nav-pills" >
                                            {
                                                categorys.map((data,key)=>{
                                                    return <li key={`article_${key}`}>
                                                        <a style={{cursor:'pointer'}} className="nav-link" 
                                                            onClick={()=>{this.selectCategory(data)}}>{data.name}</a>
                                                        </li>
                                                })
                                            }
                                            
                                        </div>
                                    </div>
                                    
                                    <div className="col-md-12 d-flex align-items-center">
                                        
                                        <div className="tab-content ftco-animate" >
                                            
                                            <div className="tab-pane fade show active">
                                                <div className="row">
                                                    {
                                                        publicationsLimitFour.map((data,key)=>{
                                                            return <div key={`article_${key}`} className="col-md-4 text-center">
                                                                        <div className="menu-wrap">
                                                                            <a href="#" className="menu-img img mb-4"
                                                                                style={{ backgroundImage: `url("${data.urlfile}")` }}></a>
                                                                            <div className="text">
                                                                                <h3><a href="#">{data.title}</a></h3>
                                                                                <p>{data.description}.</p>
                                                                                <p className="price"><span>precio: {data.price}bs</span></p>
                                                                                
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                        })
                                                    }
                                                    
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                <section className="ftco-gallery">
                    <div className="container-wrap">
                        <div className="row no-gutters">
                            <div className="col-md-3 ftco-animate">
                                <a href="#" className="gallery img d-flex align-items-center"
                                    style={{ backgroundImage: `url("/images/gallery-1.jpg")` }}>
                                    <div className="icon mb-4 d-flex align-items-center justify-content-center">
                                        <span className="icon-search"></span>
                                    </div>
                                </a>
                            </div>
                            <div className="col-md-3 ftco-animate">
                                <a href="#" className="gallery img d-flex align-items-center"
                                    style={{ backgroundImage: `url("/images/gallery-2.jpg")` }}>
                                    <div className="icon mb-4 d-flex align-items-center justify-content-center">
                                        <span className="icon-search"></span>
                                    </div>
                                </a>
                            </div>
                            <div className="col-md-3 ftco-animate">
                                <a href="#" className="gallery img d-flex align-items-center"
                                    style={{ backgroundImage: `url("/images/gallery-3.jpg")` }}>
                                    <div className="icon mb-4 d-flex align-items-center justify-content-center">
                                        <span className="icon-search"></span>
                                    </div>
                                </a>
                            </div>
                            <div className="col-md-3 ftco-animate">
                                <a href="#" className="gallery img d-flex align-items-center"
                                    style={{ backgroundImage: `url("/images/gallery-4.jpg")` }}>
                                    <div className="icon mb-4 d-flex align-items-center justify-content-center">
                                        <span className="icon-search"></span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="ftco-appointment">
                    <div className="overlay"></div>
                    <div className="container-wrap">
                        <div className="row no-gutters d-md-flex align-items-center">
                            <div className="col-md-6 d-flex align-self-stretch">
                                <div id="map"></div>
                            </div>
                            <div className="col-md-6 appointment ftco-animate">
                                <h3 className="mb-3">Contact Us</h3>
                                <form action="#" className="appointment-form">
                                    <div className="d-md-flex">
                                        <div className="form-group">
                                            <input type="text" className="form-control"
                                                placeholder="First Name"></input>
                                        </div>
                                    </div>
                                    <div className="d-me-flex">
                                        <div className="form-group">
                                            <input type="text" className="form-control" placeholder="Last Name" />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <textarea name="" id="" cols="30" rows="3" className="form-control"
                                            placeholder="Message"></textarea>
                                    </div>
                                    <div className="form-group">
                                        <input type="submit" value="Send" className="btn btn-primary py-3 px-4" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        )
    }
}

export default withTracker((props)=>{
    const optiospublications = {
        category : categorySelect.get()
    }
    const subscriptionPublications = Meteor.subscribe('publications',optiospublications,'getAllPublications')
    const subcriptionCategory = Meteor.subscribe('category',{},'getCategory')
    const categorys = categoryClass.find().fetch()
    const publications = publicationsClass.find({},{sort:{createdAt:-1}}).fetch()
    const publicationsLimitFour = publicationsClass.find({},{sort:{createdAt:-1},limit:6}).fetch()
    return {publications,subscriptionPublications,categorys,subcriptionCategory,publicationsLimitFour}
})(Main)