import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';



export default class Signup extends Component {
  constructor(props){
    super(props)
    this.state={
        nameComplete:'',
        username:'',
        email:'',
        password:'',
        re_password:''
    }
  }
  SubmitForRegister = (e) =>{
    e.preventDefault()
    console.log('Enviando Formulario de Registro')
    const form = {
      nameComplete:this.state.nameComplete,
      email:this.state.email,
      username:this.state.username,
      password:this.state.password,
      re_password:this.state.re_password
    }
    Meteor.call('userCreate',form,function(error,resp){
      if(error){
          alert(error.reason)
      }else{
          alert(resp)
      }
    })
  }

  
  componentDidMount(){
    import '/imports/assets/principal/css'
    import '/imports/assets/principal/js'
  }
  render() {
    return (
        <div> 
           
            <section className="ftco-section contact-section">
              <div className="container mt-5">
                <div className="row block-9">

                  <div className="col-md-1"></div>
                  <div className="col-md-6 ftco-animate">
                    <form action="#" className="contact-form" onSubmit={this.SubmitForRegister}>
                      <div className="row">
                      <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="name">Nombre completo</label>
                            <input type="text" className="form-control" name="name" placeholder="escriba aqui su nombre..." autoComplete="off" onChange={e => this.setState({nameComplete:e.target.value})}/>
                          </div>
                          
                        </div>
                        <div className="col-md-6">
                          <div className="form-group">
                            <label htmlFor="name">Nombre del negocio</label>
                              <input type="text" className="form-control" name="username" placeholder="este sera su usuario" autoComplete="off" onChange={e => this.setState({username:e.target.value})}/>
                          </div>
                        </div>
                        
                      </div>
                      
                      <div className="form-group">
                        <label htmlFor="name">Correo Electronico</label>
                        <input type="text" className="form-control" name="email" placeholder="Correo Electronico" autoComplete="off" onChange={e => this.setState({email:e.target.value})}/>
                      </div>
                      
                      
                      <div className="form-group">
                        <label htmlFor="name">Contraseña</label>
                         <input className="form-control" type="password" name="password" placeholder="contraseña" onChange={e => this.setState({password:e.target.value})}/>
                      </div>
                      <div className="form-group">
                        <label htmlFor="name">Confirme contraseña</label>
                         <input className="form-control" type="password" name="re-password" placeholder="confirme contraseña"  onChange={e => this.setState({re_password:e.target.value})}/>
                      </div>
                      <div className="form-group">
                        <input type="submit" value="Registrarse" className="btn btn-primary py-3 px-5"/>
                      </div>
                      <a href="/bienvenido/login" className="registration">Iniciar Sesion</a>
                    </form>
                  </div>
                </div>
              </div>
            </section>

            <section className="one-half img"  style={{ backgroundImage: `url("/images/bg_1.jpg")` }}>
              <div className="slider-item" >
                <div className="overlay"></div>
                <div className="container">
                  <div className="row slider-text justify-content-center align-items-center">

                    <div className="col-md-7 col-sm-12 text-center ftco-animate">
                      <h1 className="mb-3 mt-5 bread">Registrate y publica tus servicios :)</h1>
                    
                    </div>

                  </div>
                </div>
              </div>
            </section>

        </div>
    );
  }
}
