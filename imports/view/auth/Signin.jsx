import React, { Component } from 'react';
import {Link} from 'react-router-dom'
import { Meteor } from 'meteor/meteor';

export default class Signin extends Component {
  
  constructor(props){
    super(props)
    this.state={
        username:'',
        password:''
    }
  }
  SubmitForlogin = (e) =>{
    e.preventDefault()
    const mthis = this
    Meteor.loginWithPassword(this.state.username,this.state.password,function(error){
        if(error)
            console.log(error)
        else
            window.location.replace('/dashboard/home')
    })
  }


  componentDidMount(){
    import '/imports/assets/principal/css'
    import '/imports/assets/principal/js'
  }
  render() {
    return (
        <div> 
           
            <section className="home-slider owl-carousel img"  style={{ backgroundImage: `url("/images/login.jpg")` }}>
              <div className="container mt-6">
                <div className="row block-9">
                <div className="col-md-1"></div>
                  <div className="col-md-6 ftco-animate">
                    <form className="contact-form" onSubmit={this.SubmitForlogin}>
                      <div className="ftco-appointment">
                        
                        <h2>Ingrese Aqui</h2>
                        <div className="form-group">
                          <label htmlFor="name">Correo Electronico</label>
                          <input className="form-control" type="text" name="email" placeholder="Email" onChange={e=>
                          this.setState({username:e.target.value})}/>
                        </div>
                        <div className="form-group">
                          <label htmlFor="name">Password</label>
                          <input className="form-control" type="password" name="password" placeholder="Password"
                            onChange={e=>
                          this.setState({password:e.target.value})}/>
                        </div>
                        <div className="form-group">
                          <input type="submit" name="submit" value="Iniciar" className="btn btn-primary py-3 px-5" />
                        </div>

                        <a href="#" className="forget">¿Olvidaste tu contraseña?</a><br />
                        <a href="/bienvenido/register" className="registration">Registrate</a>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </section>

            

        </div>
    );
  }
}
