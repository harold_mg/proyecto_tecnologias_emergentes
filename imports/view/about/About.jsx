import React, { Component } from 'react'

export default class About extends Component {
    render() {
        return (
            <div>
                <div>
                    <section className="home-slider owl-carousel img" style={{backgroundImage: 'url(/images/bg_1.jpg)'}}>
                        <div className="slider-item" style={{backgroundImage: 'url(images/bg_3.jpg)'}}>
                        <div className="overlay" />
                        <div className="container">
                            <div className="row slider-text justify-content-center align-items-center">
                            <div className="col-md-7 col-sm-12 text-center ftco-animate">
                                <h1 className="mb-3 mt-5 bread">About</h1>
                                <p className="breadcrumbs"><span className="mr-2"><a href="index.html">Home</a></span>
                                <span>About</span></p>
                            </div>
                            </div>
                        </div>
                        </div>
                    </section>
                    <section className="ftco-intro">
                        <div className="container-wrap">
                        <div className="wrap d-md-flex">
                            <div className="info">
                            <div className="row no-gutters">
                                <div className="col-md-4 d-flex ftco-animate">
                                <div className="icon"><span className="icon-phone" /></div>
                                <div className="text">
                                    <h3>000 (123) 456 7890</h3>
                                    <p>A small river named Duden flows</p>
                                </div>
                                </div>
                                <div className="col-md-4 d-flex ftco-animate">
                                <div className="icon"><span className="icon-my_location" /></div>
                                <div className="text">
                                    <h3>198 West 21th Street</h3>
                                    <p>Suite 721 New York NY 10016</p>
                                </div>
                                </div>
                                <div className="col-md-4 d-flex ftco-animate">
                                <div className="icon"><span className="icon-clock-o" /></div>
                                <div className="text">
                                    <h3>Open Monday-Friday</h3>
                                    <p>8:00am - 9:00pm</p>
                                </div>
                                </div>
                            </div>
                            </div>
                            <div className="social d-md-flex pl-md-5 p-4 align-items-center">
                            <ul className="social-icon">
                                <li className="ftco-animate"><a href="#"><span className="icon-twitter" /></a></li>
                                <li className="ftco-animate"><a href="#"><span className="icon-facebook" /></a></li>
                                <li className="ftco-animate"><a href="#"><span className="icon-instagram" /></a></li>
                            </ul>
                            </div>
                        </div>
                        </div>
                    </section>
                    <section className="ftco-about d-md-flex">
                        <div className="one-half img" style={{backgroundImage: 'url(/images/about.jpg)'}} />
                        <div className="one-half ftco-animate">
                        <div className="heading-section ftco-animate ">
                            <h2 className="mb-4">Welcome to <span className="flaticon-pizza">Pizza</span> A Restaurant</h2>
                        </div>
                        <div>
                            <p>On her way she met a copy. The copy warned the Little Blind Text, that where it came from
                            it would have been rewritten a thousand times and everything that was left from its
                            origin would be the word "and" and the Little Blind Text should turn around and return
                            to its own, safe country. But nothing the copy said could convince her and so it didn’t
                            take long until a few insidious Copy Writers ambushed her, made her drunk with Longe and
                            Parole and dragged her into their agency, where they abused her for their.</p>
                        </div>
                        </div>
                    </section>
                    <section className="ftco-section">
                        <div className="container">
                        <div className="row justify-content-center mb-5 pb-3">
                            <div className="col-md-7 heading-section ftco-animate text-center">
                            <h2 className="mb-4">Our Chef</h2>
                            <p className="flip"><span className="deg1" /><span className="deg2" /><span className="deg3" /></p>
                            <p className="mt-5">Far far away, behind the word mountains, far from the countries Vokalia
                                and Consonantia, there live the blind texts.</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-3 d-flex mb-sm-4 ftco-animate">
                            <div className="staff">
                                <div className="img mb-4" style={{backgroundImage: 'url(/images/person_1.jpg)'}} />
                                <div className="info text-center">
                                <h3><a href="teacher-single.html">Tom Smith</a></h3>
                                <span className="position">Hair Specialist</span>
                                <div className="text">
                                    <p>Far far away, behind the word mountains, far from the countries Vokalia
                                    and Consonantia, there live the blind texts.</p>
                                </div>
                                </div>
                            </div>
                            </div>
                            <div className="col-lg-3 d-flex mb-sm-4 ftco-animate">
                            <div className="staff">
                                <div className="img mb-4" style={{backgroundImage: 'url(/images/person_2.jpg)'}} />
                                <div className="info text-center">
                                <h3><a href="teacher-single.html">Mark Wilson</a></h3>
                                <span className="position">Beard Specialist</span>
                                <div className="text">
                                    <p>Far far away, behind the word mountains, far from the countries Vokalia
                                    and Consonantia, there live the blind texts.</p>
                                </div>
                                </div>
                            </div>
                            </div>
                            <div className="col-lg-3 d-flex mb-sm-4 ftco-animate">
                            <div className="staff">
                                <div className="img mb-4" style={{backgroundImage: 'url(/images/person_3.jpg)'}} />
                                <div className="info text-center">
                                <h3><a href="teacher-single.html">Patrick Jacobson</a></h3>
                                <span className="position">Hair Stylist</span>
                                <div className="text">
                                    <p>Far far away, behind the word mountains, far from the countries Vokalia
                                    and Consonantia, there live the blind texts.</p>
                                </div>
                                </div>
                            </div>
                            </div>
                            <div className="col-lg-3 d-flex mb-sm-4 ftco-animate">
                            <div className="staff">
                                <div className="img mb-4" style={{backgroundImage: 'url(/images/person_4.jpg)'}} />
                                <div className="info text-center">
                                <h3><a href="teacher-single.html">Ivan Dorchsner</a></h3>
                                <span className="position">Beard Specialist</span>
                                <div className="text">
                                    <p>Far far away, behind the word mountains, far from the countries Vokalia
                                    and Consonantia, there live the blind texts.</p>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </section>
                    <section className="ftco-counter ftco-bg-dark img" id="section-counter" style={{backgroundImage: 'url(/images/bg_2.jpg)'}} data-stellar-background-ratio="0.5">
                        <div className="overlay" />
                        <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-md-10">
                            <div className="row">
                                <div className="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                                <div className="block-18 text-center">
                                    <div className="text">
                                    <div className="icon"><span className="flaticon-pizza-1" /></div>
                                    <strong className="number" data-number={100}>0</strong>
                                    <span>Pizza Branches</span>
                                    </div>
                                </div>
                                </div>
                                <div className="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                                <div className="block-18 text-center">
                                    <div className="text">
                                    <div className="icon"><span className="flaticon-medal" /></div>
                                    <strong className="number" data-number={85}>0</strong>
                                    <span>Number of Awards</span>
                                    </div>
                                </div>
                                </div>
                                <div className="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                                <div className="block-18 text-center">
                                    <div className="text">
                                    <div className="icon"><span className="flaticon-laugh" /></div>
                                    <strong className="number" data-number={10567}>0</strong>
                                    <span>Happy Customer</span>
                                    </div>
                                </div>
                                </div>
                                <div className="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                                <div className="block-18 text-center">
                                    <div className="text">
                                    <div className="icon"><span className="flaticon-chef" /></div>
                                    <strong className="number" data-number={900}>0</strong>
                                    <span>Staff</span>
                                    </div>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </section>
                    <section className="ftco-appointment">
                        <div className="overlay" />
                        <div className="container-wrap">
                        <div className="row no-gutters d-md-flex align-items-center">
                            <div className="col-md-6 d-flex align-self-stretch">
                            <div id="map" />
                            </div>
                            <div className="col-md-6 appointment ftco-animate">
                            <h3 className="mb-3">Contact Us</h3>
                            <form action="#" className="appointment-form">
                                <div className="d-md-flex">
                                <div className="form-group">
                                    <input type="text" className="form-control" placeholder="First Name" />
                                </div>
                                </div>
                                <div className="d-me-flex">
                                <div className="form-group">
                                    <input type="text" className="form-control" placeholder="Last Name" />
                                </div>
                                </div>
                                <div className="form-group">
                                {/*<textarea name id cols={30} rows={3} className="form-control" placeholder="Message" defaultValue={""} />*/}
                                </div>
                                <div className="form-group">
                                <input type="submit" defaultValue="Send" className="btn btn-primary py-3 px-4" />
                                </div>
                            </form>
                            </div>
                        </div>
                        </div>
                    </section>
                </div>


            </div>
        )
    }
}
