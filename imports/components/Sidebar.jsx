import React, { Component } from 'react'
import {Link} from 'react-router-dom'

export default class Sidebar extends Component {
    render() {
        return (
            <div>
                <div className="main-sidebar sidebar-style-2">
                    <aside id="sidebar-wrapper">
                        <div className="sidebar-brand">
                            <Link to="/dashboard/home">
                                <img alt="image" src="/dashboard/img/logo.png"
                                        className="header-logo" /> <span className="logo-name">Otika</span>
                            </Link>
                        </div>
                        <ul className="sidebar-menu">
                            <li className="menu-header">Main</li>
                            <li className="dropdown active">
                                <Link className="nav-link" to="/dashboard/home"><i data-feather="monitor"></i><span>Panel de control</span></Link>
                            </li>
                            <li className="dropdown">
                            <a href="#" onClick={e => e.preventDefault()} className="menu-toggle nav-link has-dropdown"><i data-feather="briefcase"></i><span>PUBLICACIONES</span></a>
                                <ul className="dropdown-menu">
                                    <li>
                                        <Link className="nav-link" to="/dashboard/create-publications">Crear Publicacion</Link>
                                    </li>
                                    <li>
                                        <Link className="nav-link" to="/dashboard/user-publications">Mis Publicaciones</Link>
                                    </li>                
                                </ul>
                            </li>
                            <li className="dropdown">
                                <a href="#" className="menu-toggle nav-link has-dropdown"><i
                                        data-feather="command"></i><span>CATEGORIAS</span></a>
                                <ul className="dropdown-menu">
                                    <li>
                                        <Link className="nav-link" to="/dashboard/create-category">Crear Categoria</Link>
                                    </li>
                                    <li>
                                        <Link className="nav-link" to="/dashboard/all-category">Ver mis Categorias</Link>
                                    </li>
                                </ul>
                            </li>

                           
                        </ul>
                    </aside>
                </div>

            </div>
        )
    }
}
